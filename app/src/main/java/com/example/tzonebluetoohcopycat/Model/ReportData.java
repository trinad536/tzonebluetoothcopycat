package com.example.tzonebluetoohcopycat.Model;

import com.TZONE.Bluetooth.Temperature.Model.Device;
import com.TZONE.Bluetooth.Utils.DateUtil;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ReportData {

    public String SN;

    public double Temperature;

    public double Humidity;

    public Date RecordTime;

    public Date ServerTime;

    public ReportData(){}
    public ReportData(Device device){
        this.SN = device.SN;
        this.Temperature = device.Temperature;
        this.Humidity = device.Humidity;
        this.RecordTime = device.UTCTime;
        this.ServerTime = DateUtil.GetUTCTime();
    }


}
