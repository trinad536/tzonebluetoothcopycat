package com.example.tzonebluetoohcopycat;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.TZONE.Bluetooth.BLE;
import com.TZONE.Bluetooth.ILocalBluetoothCallBack;
import com.TZONE.Bluetooth.Temperature.BroadcastService;
import com.TZONE.Bluetooth.Temperature.Model.Device;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class ScanActivity extends AppCompatActivity {
    private TextView txtPrint;

    private BluetoothAdapter _BluetoothAdapter;
    private BroadcastService _BroadcastService;
    private boolean _IsInit = false;
    private Timer _Timer;
    private List<Device> _DeviceList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan);

        txtPrint = (TextView)findViewById(R.id.txtPrint);

        try{
            if(_BroadcastService == null){
                _BroadcastService = new BroadcastService();
            }
            final BluetoothManager bluetoothManager =
                    (BluetoothManager) getSystemService(this.BLUETOOTH_SERVICE);
            _BluetoothAdapter = bluetoothManager.getAdapter();
            if(_BroadcastService.Init(_BluetoothAdapter,_LocalBluetoothCallBack)){
                _IsInit = true;
            }else {
                _IsInit = false;
                Toast.makeText(ScanActivity.this, "Native Bluetooth hardware or driver was not found!", Toast.LENGTH_SHORT).show();
                return;
            }

        }catch (Exception ex){
            Toast.makeText(ScanActivity.this, ex.toString(), Toast.LENGTH_SHORT).show();
        }
        txtPrint.setText("Scanning ....");
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            if(_Timer != null)
                _Timer.cancel();
            _Timer = new Timer();
            TimerTask timerTask = new TimerTask() {
                @Override
                public void run() {
                    try {
                        synchronized (this) {
                            if (_IsInit) {
                                _BroadcastService.StartScan();
                            }
                        }
                    }catch (Exception ex){}
                }
            };
            _Timer.schedule(timerTask, 1000, 500);
        }catch (Exception ex){}
    }

    @Override
    protected void onDestroy() {
        try {
            _Timer.cancel();
            if(_BroadcastService!=null)
                _BroadcastService.StopScan();
        }catch (Exception ex){}
        super.onDestroy();
    }

    Date LastUpdateTime = new Date();
    private void AddOrUpdate(final BLE ble){
        try {
            Device d = new Device();
            d.fromScanData(ble);
            if(d == null || d.SN == null || d.SN.length() != 8)
                return;

            /*
                HardwareModel = "39"  BT04
                HardwareModel = "3A"  BT05
            */
            boolean flag = true;
            for (int i = 0; i < _DeviceList.size(); i++) {
                Device item = _DeviceList.get(i);
                if (item.SN.equals(d.SN)){
                    flag = false;
                }
            }
            if(flag){
                _DeviceList.add(d);
            }

            Date now = new Date();
            long Totaltime = (now.getTime() - LastUpdateTime.getTime());
            if (Totaltime > 3000) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        synchronized (this) {
                            String printText = "";
                            for (int i = 0; i < _DeviceList.size(); i++) {
                                printText += "" + (i+1) + "、SN:" + _DeviceList.get(i).SN +" Temperature:" + (_DeviceList.get(i).Temperature != - 1000 ? _DeviceList.get(i).Temperature : "--") +"℃  Humidity:" + (_DeviceList.get(i).Humidity != -1000 ? _DeviceList.get(i).Humidity : "--") + "% Battery:"+_DeviceList.get(i).Battery+"%";
                                printText += "\n\n";
                            }
                            txtPrint.setText(printText);
                        }
                    }
                });
                LastUpdateTime = now;
            }
        }catch (Exception ex){
            Log.e("home", "AddOrUpdate:" + ex.toString());
        }
    }


    public ILocalBluetoothCallBack _LocalBluetoothCallBack = new ILocalBluetoothCallBack() {

        @Override
        public void OnEntered(BLE ble) {
            try{
                AddOrUpdate(ble);
            }catch (Exception ex) {}
        }

        @Override
        public void OnUpdate(BLE ble) {
            try {
                AddOrUpdate(ble);
            }catch (Exception ex) {}
        }

        @Override
        public void OnExited(final BLE ble) {
            try{

            }catch (Exception ex) {}
        }

        @Override
        public void OnScanComplete() {

        }
    };


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_scan, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
