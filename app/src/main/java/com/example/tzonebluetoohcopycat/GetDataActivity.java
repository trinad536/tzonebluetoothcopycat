package com.example.tzonebluetoohcopycat;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.TZONE.Bluetooth.AppBase;
import com.TZONE.Bluetooth.BLE;
import com.TZONE.Bluetooth.BLEGattService;
import com.TZONE.Bluetooth.IConfigCallBack;
import com.TZONE.Bluetooth.ILocalBluetoothCallBack;
import com.TZONE.Bluetooth.Temperature.BroadcastService;
import com.TZONE.Bluetooth.Temperature.ConfigService;
import com.TZONE.Bluetooth.Temperature.Model.CharacteristicHandle;
import com.TZONE.Bluetooth.Temperature.Model.CharacteristicType;
import com.TZONE.Bluetooth.Temperature.Model.Device;
import com.TZONE.Bluetooth.Utils.AppUtil;
import com.TZONE.Bluetooth.Utils.BinaryUtil;
import com.TZONE.Bluetooth.Utils.DateUtil;
import com.TZONE.Bluetooth.Utils.StringConvertUtil;
import com.TZONE.Bluetooth.Utils.StringUtil;

import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.UUID;

import  com.example.tzonebluetoohcopycat.Model.*;

public class GetDataActivity extends AppCompatActivity {

    private TextView txtPrint;

    private ProgressDialog _ProgressDialog;
    private Dialog _AlertDialog;

    private String SN = "";
    private String Token = "000000";
    private String DeviceName = "";
    private String HardwareModel = "3A01";
    private String Firmware = "";
    private Report _Report;
    private BluetoothAdapter _BluetoothAdapter;
    private BroadcastService _BroadcastService;
    private ConfigService _ConfigService;
    private boolean _IsInit = false;
    private boolean _IsScanning = false;
    private boolean _IsConnecting = false;
    private boolean _IsReading = false;
    private boolean _IsSync = false;
    private boolean _IsSynced = false;
    public Queue<byte[]> Buffer = new LinkedList<byte[]>();
    private int _SyncCount = 0;
    private int _SyncIndex = 0;
    private int _SyncProgress = 0;
    private int _SyncMode = 0;
    private int _SyncDateTime = 0;
    private Date _SyncBeginTime = new Date(new Date().getTime() - (24 * 60 * 60 * 1000));
    private Date _SyncEndTime = new Date();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_data);

        txtPrint = (TextView)findViewById(R.id.txtPrint);
        txtPrint.setText("Please wait...");

        InputToken();
    }

    //Input Password
    public void InputToken(){
        final LinearLayout layout = new LinearLayout(GetDataActivity.this);
        layout.setOrientation(LinearLayout.VERTICAL);
        final EditText txtSN2 = new EditText(GetDataActivity.this);
        final EditText txtToken2 = new EditText(GetDataActivity.this);
        txtSN2.setHint("SN");
        layout.addView(txtSN2);
        layout.addView(txtToken2);
        txtToken2.setText(Token+"");
        new AlertDialog.Builder(GetDataActivity.this)
                .setTitle("Please enter SN and password")
                .setView(layout)
                .setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        // TODO 自动生成的方法存根
                        try {
                            SN = txtSN2.getText().toString();
                            Token = txtToken2.getText().toString();

                            if(SN.isEmpty())
                                finish();

                            _Report = new Report();
                            _Report.SN = SN;
                        } catch (Exception ex) {
                            _AlertDialog = new AlertDialog.Builder(GetDataActivity.this).
                                    setTitle("Message")
                                    .setMessage("SN or Password is error!")
                                    .setPositiveButton("Confirm", null)
                                    .show();
                        }
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Scan();
                            }
                        });
                    }
                })
                .setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        finish();
                    }
                })
                .show();
    }

    //Select Mode
    public void InputSyncMode(final Device device){
        try {
            if(_ProgressDialog!=null && _ProgressDialog.isShowing())
                _ProgressDialog.dismiss();

            _SyncMode = 0;
            if((device.HardwareModel.equals("3A01") && Integer.parseInt(device.Firmware) >= 12)
                    || (device.HardwareModel.equals("3901") && Integer.parseInt(device.Firmware) >= 25)
                    || (device.HardwareModel.equals("3C01") && Integer.parseInt(device.Firmware) >= 26)
                    || (device.HardwareModel.equals("3A04") && Integer.parseInt(device.Firmware) >= 13)){
                if(AppBase.IsFactory){
                    Connect(device);
                }else {
                    _SyncMode = 1;
                    InputSyncDateTime(device);
                }
            }else {
                Connect(device);
            }
        }catch (Exception ex){}
    }
    //Select the extract time period
    public void InputSyncDateTime(final Device device){
        try {
            new AlertDialog.Builder(GetDataActivity.this).setTitle("Select the extract time period")
                    .setSingleChoiceItems(new String[]{"All","1 day","3 day","7 day","30 day","Set the time range"},1,new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            try {
                                _SyncDateTime = which;
                                dialog.dismiss();
                                if(which == 5){
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            InputSyncDateTimeValue(device);
                                        }
                                    });
                                }else {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Connect(device);
                                        }
                                    });
                                }
                            }catch (Exception ex){}
                        }
                    })
                    .setOnCancelListener(new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialog) {
                            finish();
                        }
                    })
                    .show();
        }catch (Exception ex){}
    }
    public void InputSyncDateTimeValue(final Device device){
        try {
            LinearLayout linearLayout = new LinearLayout(GetDataActivity.this);
            linearLayout.setOrientation(LinearLayout.VERTICAL);
            ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            linearLayout.setLayoutParams(layoutParams);

            LinearLayout linearLayout2 = new LinearLayout(GetDataActivity.this);
            linearLayout2.setOrientation(LinearLayout.HORIZONTAL);
            linearLayout2.setLayoutParams(new ViewGroup.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            TextView labBeginTime = new TextView(GetDataActivity.this);
            labBeginTime.setText("  " + "BeginTime");
            final EditText txtBeginTime = new EditText(GetDataActivity.this);
            txtBeginTime.setLayoutParams(new ViewGroup.LayoutParams(AppUtil.dip2px(GetDataActivity.this, 200),ViewGroup.LayoutParams.WRAP_CONTENT));
            linearLayout2.addView(labBeginTime);
            linearLayout2.addView(txtBeginTime);
            linearLayout.addView(linearLayout2);

            LinearLayout linearLayout3 = new LinearLayout(GetDataActivity.this);
            linearLayout3.setOrientation(LinearLayout.HORIZONTAL);
            linearLayout3.setLayoutParams(new ViewGroup.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            TextView labEndTime = new TextView(GetDataActivity.this);
            labEndTime.setText("  " + "EndTime");
            final EditText txtEndTime = new EditText(GetDataActivity.this);
            txtEndTime.setLayoutParams(new ViewGroup.LayoutParams(AppUtil.dip2px(GetDataActivity.this,200),ViewGroup.LayoutParams.WRAP_CONTENT));
            linearLayout3.addView(labEndTime);
            linearLayout3.addView(txtEndTime);
            linearLayout.addView(linearLayout3);

            txtBeginTime.setText(DateUtil.ToString(_SyncBeginTime,"yyyy-MM-dd HH:mm:ss"));
            txtEndTime.setText(DateUtil.ToString(_SyncEndTime, "yyyy-MM-dd HH:mm:ss"));
            _AlertDialog = new AlertDialog.Builder(GetDataActivity.this).setTitle("Set the time range")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setView(linearLayout)
                    .setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            try {
                                final Date beginTime = DateUtil.ToData(txtBeginTime.getText().toString(), "yyyy-MM-dd HH:mm:ss");
                                final Date endTime = DateUtil.ToData(txtEndTime.getText().toString(), "yyyy-MM-dd HH:mm:ss");
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (beginTime.getTime() > endTime.getTime()) {
                                            _AlertDialog = new AlertDialog.Builder(GetDataActivity.this).
                                                    setTitle("Message")
                                                    .setMessage("Set period of time than range")
                                                    .setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                                                        @Override
                                                        public void onClick(DialogInterface dialog, int which) {
                                                            finish();
                                                        }
                                                    })
                                                    .setCancelable(false)
                                                    .show();
                                        } else {
                                            _SyncBeginTime = beginTime;
                                            _SyncEndTime = endTime;
                                            Connect(device);
                                        }
                                    }
                                });
                            } catch (Exception ex) {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        _AlertDialog = new AlertDialog.Builder(GetDataActivity.this).
                                                setTitle("Message")
                                                .setMessage("Time format is not error!(yyyy-MM-dd HH:mm:ss)")
                                                .setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        finish();
                                                    }
                                                })
                                                .setCancelable(false)
                                                .show();
                                    }
                                });
                            }
                        }
                    })
                    .setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    InputSyncDateTime(device);
                                }
                            });
                        }
                    })
                    .setCancelable(false)
                    .show();
        }catch (Exception ex){}
    }

    /**
     * Scan
     */
    protected void Scan(){
        try {
            if(SN == null || SN.equals(""))
                finish();

            if(_BroadcastService == null){
                _BroadcastService = new BroadcastService();
            }
            final BluetoothManager bluetoothManager =
                    (BluetoothManager) getSystemService(this.BLUETOOTH_SERVICE);
            _BluetoothAdapter = bluetoothManager.getAdapter();
            if(_BroadcastService.Init(_BluetoothAdapter,_LocalBluetoothCallBack)){
                _IsInit = true;
            }else {
                _IsInit = false;
                Toast.makeText(GetDataActivity.this, "Native Bluetooth hardware or driver was not found!", Toast.LENGTH_SHORT).show();
                return;
            }

            if(_IsScanning)
                return;

            _IsScanning = true;
            _BroadcastService.StartScan();

            if(_ProgressDialog!=null && _ProgressDialog.isShowing())
                _ProgressDialog.dismiss();
            _ProgressDialog = new ProgressDialog(this).show(this,"","Searching for devices...",true,true,new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    Toast.makeText(GetDataActivity.this, "Could not find the device!", Toast.LENGTH_SHORT).show();
                    finish();
                }
            });
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(15000);
                        if(_IsScanning){
                            _BroadcastService.StopScan();
                            _ProgressDialog.cancel();
                            _IsScanning = false;
                        }
                    }catch (Exception ex){}
                }
            }).start();


        }catch (Exception ex){
            _IsInit = false;
            _IsScanning = false;
            Toast.makeText(GetDataActivity.this, "Could not find the device!" + " ex:"+ex.toString(), Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    /**
     * Connect
     */
    protected void Connect(Device device){
        try {
            if(_IsConnecting)
                return;
            _BroadcastService.StopScan();
            _IsScanning = false;

            if(device != null) {
                /*if(device.Temperature != -1000)
                    labTemp2.setText(device.Temperature + " ℃");*/
                if(device.Name != null && !device.Name.equals(""))
                    DeviceName = device.Name;
                HardwareModel = device.HardwareModel;
                Firmware = device.Firmware;

                if(device.Battery != 1000)
                    _Report.Battery = device.Battery;
                if(device.HardwareModel.equals("3901"))
                    _Report.ProductType = "BT04";
                else  if(device.HardwareModel.equals("3A01"))
                    _Report.ProductType = "BT05";
                else if(device.HardwareModel.equals("3C01"))
                    _Report.ProductType = "BT04B";
                else  if(device.HardwareModel.equals("3A04"))
                    _Report.ProductType = "BT05B";
                else
                    _Report.ProductType = "BT("+device.HardwareModel+")";
                _Report.FirmwareVersion = device.Firmware;
            }

            _IsConnecting = true;
            if(_ConfigService != null){
                _ConfigService.Dispose();
            }
            _ConfigService = new ConfigService(_BluetoothAdapter,this,device.MacAddress,30000,_IConfigCallBack);

            if(_ProgressDialog!=null && _ProgressDialog.isShowing())
                _ProgressDialog.dismiss();
            _ProgressDialog = new ProgressDialog(this).show(this,"","Connecting...",true,true,new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    Toast.makeText(GetDataActivity.this, "Unable to connect the device!", Toast.LENGTH_SHORT).show();
                    finish();
                }
            });
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(30000);
                        if(_IsConnecting) {
                            _ProgressDialog.cancel();
                            _IsConnecting = false;
                        }
                    }catch (Exception ex){}
                }
            }).start();
        }catch (Exception ex){
            _IsConnecting = false;
            Toast.makeText(GetDataActivity.this, "Unable to connect the device!" + " ex:"+ex.toString(), Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    /**
     * Read Config
     */
    protected void ReadConfig(){
        try {
            if(_IsReading)
                return;

            _IsConnecting = false;


            _IsReading = true;
            if(HardwareModel.equals("3901"))
                _ConfigService.ReadConfig_BT04(Firmware);
            else if(HardwareModel.equals("3C01"))
                _ConfigService.ReadConfig_BT04B(Firmware);
            else if(HardwareModel.equals("3A04"))
                _ConfigService.ReadConfig_BT05B(Firmware);
            else
                _ConfigService.ReadConfig_BT05(Firmware);

            if(_ProgressDialog!=null && _ProgressDialog.isShowing())
                _ProgressDialog.dismiss();
            _ProgressDialog = new ProgressDialog(this).show(this,"","Reading device parameters...",true,true,new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    Toast.makeText(GetDataActivity.this, "Can not read the device parameters!", Toast.LENGTH_SHORT).show();
                    finish();
                }
            });
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(15000);
                        if(_IsReading){
                            _ProgressDialog.cancel();
                            _IsReading = false;
                        }
                    }catch (Exception ex){}
                }
            }).start();

        }catch (Exception ex){
            _IsReading = false;
            Toast.makeText(GetDataActivity.this, "Can not read the device parameters!" + " ex:"+ex.toString(), Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    /**
     * Sync Data
     */
    protected void SyncData(Device device){
        try {
            if(_IsSync)
                return;

            _IsReading = false;

            _SyncIndex = 0;
            _SyncCount = 0;
            _SyncProgress = 0;
            Buffer.clear();

            if(device != null) {
                if(device.Name != null && !device.Name.equals(""))
                    DeviceName = device.Name;
                if(device.SavaCount != 1000) {
                    _SyncCount = device.SavaCount;
                }

                _Report.Name = DeviceName + "("+device.SN+") -- "+ DateUtil.ToString(DateUtil.GetUTCTime(),"yyyyMMdd");
                if(device.Notes != null)
                    _Report.Notes = device.Notes;
                if(device.Description != null)
                    _Report.Description = device.Description;

                _Report.SamplingInterval = device.SaveInterval; //device.SamplingInterval;
                _Report.LT = device.LT;
                _Report.HT = device.HT;
            }

            if(device.SavaCount == -1000 || device.SavaCount == 0){
                Toast.makeText(GetDataActivity.this, "Data record is empty! Without extracting", Toast.LENGTH_SHORT).show();
                finish();
            }

            _IsSync = true;
            _ConfigService.Sync(true);

            if(_ProgressDialog!=null && _ProgressDialog.isShowing())
                _ProgressDialog.dismiss();

            _ProgressDialog = new ProgressDialog(this).show(this,"","Fetching data,please wait...",true,false,null);
            _ProgressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    Toast.makeText(GetDataActivity.this, "Extract the data has been canceled!", Toast.LENGTH_SHORT).show();
                    finish();
                }
            });
            _ProgressDialog.show();
            _ProgressDialog.setProgress(0);
        }catch (Exception ex){
            _IsSync = false;
            Toast.makeText(GetDataActivity.this, "Extract data during abnormal!" + " ex:"+ex.toString(), Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    protected void  ShowData(){
        try {

            //上一步骤状态关闭
            _IsSync = false;
            //_ConfigService.Sync(false);
            _ConfigService.Dispose();

            if(_ProgressDialog!=null && _ProgressDialog.isShowing())
                _ProgressDialog.dismiss();

            if(Buffer == null || Buffer.size() == 0)
                return;

            _ProgressDialog = new ProgressDialog(this).show(this,"","please wait...",true,false,null);
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        double totalTemperature = 0;int totalTemperature_count = 0;
                        double totalHumidity = 0;int totalHumidity_count = 0;
                        while (Buffer.size() > 0) {
                            Device device = new Device();
                            device.HardwareModel = HardwareModel;
                            device.Firmware = Firmware;
                            device.fromNotificationData(Buffer.remove());
                            if(device !=null && device.UTCTime != null){
                                if(device.Temperature != -1000) {
                                    totalTemperature += device.Temperature;
                                    totalTemperature_count ++;
                                    if (_Report.MaxTemp == -1000)
                                        _Report.MaxTemp = device.Temperature;
                                    if (_Report.MinTemp == -1000)
                                        _Report.MinTemp = device.Temperature;

                                    if (device.Temperature > _Report.MaxTemp)
                                        _Report.MaxTemp = device.Temperature;
                                    if (device.Temperature < _Report.MinTemp)
                                        _Report.MinTemp = device.Temperature;
                                }
                                if(device.Humidity != -1000) {
                                    totalHumidity += device.Humidity;
                                    totalHumidity_count ++;
                                    if (_Report.MaxHumidity == -1000)
                                        _Report.MaxHumidity = device.Humidity;
                                    if (_Report.MinHumidity == -1000)
                                        _Report.MinHumidity = device.Humidity;

                                    if (device.Humidity > _Report.MaxHumidity)
                                        _Report.MaxHumidity = device.Humidity;
                                    if (device.Humidity < _Report.MinHumidity)
                                        _Report.MinHumidity = device.Humidity;
                                }

                                ReportData reportData = new ReportData(device);
                                _Report.Data.add(reportData);
                            }
                        }

                        _Report.DataCount = _Report.Data.size();
                        if(_Report.Data.size() > 0 ) {
                            if(totalTemperature_count > 0)
                                _Report.AvgTemp = Math.round(totalTemperature/ totalTemperature_count);
                            if(totalHumidity_count > 0)
                                _Report.AvgHumidity = Math.round(totalHumidity / totalHumidity_count);
                            _Report.BeginTime = _Report.Data.get(0).RecordTime;
                            _Report.EndTime = _Report.Data.get(_Report.Data.size() - 1).RecordTime;
                        }

                        _Report.Generate();

                        GetDataActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                String printText = "";
                                for (int i = 0; i < _Report.Data.size() && i < 50; i++) {
                                    ReportData item = _Report.Data.get(i);
                                    printText += (i + 1) + "、";
                                    double tzone = DateUtil.GetTimeZone() / 60.0;
                                    printText += DateUtil.ToString(DateUtil.DateAddHours(item.RecordTime,tzone), "yyyy-MM-dd HH:mm:ss")+" ";
                                    if(item.Temperature != -1000)
                                        printText += "Temperature:" + item.Temperature+"℃";
                                    else
                                        printText += "Temperature:--℃";
                                    if(item.Humidity != -1000)
                                        printText += "Humidity:" + item.Humidity+"%";
                                    else
                                        printText += "Humidity:--%";
                                    printText += "\n\n";

                                }
                                txtPrint.setText(printText + "......");

                                _ProgressDialog.dismiss();

                            }
                        });

                    }catch (Exception ex){
                        _ProgressDialog.dismiss();
                    }
                }
            }).start();

        }catch (Exception ex){
            Toast.makeText(GetDataActivity.this, " ex:"+ex.toString(), Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    @Override
    protected void onDestroy() {
        try {
            if(_BroadcastService!=null)
                _BroadcastService.StopScan();
            if(_ConfigService!=null)
                _ConfigService.Dispose();
        }catch (Exception ex){}
        super.onDestroy();
    }

    public ILocalBluetoothCallBack _LocalBluetoothCallBack = new ILocalBluetoothCallBack() {
        @Override
        public void OnEntered(BLE ble) {
            final Device device = new Device();
            device.fromScanData(ble);
            if(device.SN!=null && device.SN.equals(SN)){
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        InputSyncMode(device); //Connect(device);
                    }
                });
            }
        }

        @Override
        public void OnUpdate(BLE ble) {

        }

        @Override
        public void OnExited(BLE ble) {

        }

        @Override
        public void OnScanComplete() {

        }
    };

    public long _new_sync_datatime = 0;
    public long _new_sync_interval = 0;

    public IConfigCallBack _IConfigCallBack = new IConfigCallBack() {
        @Override
        public void OnReadConfigCallBack(boolean status, final HashMap<String, byte[]> uuids) {
            if(status) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Device device = _ConfigService.GetCofing(uuids);
                        SyncData(device);
                    }
                });
            }
        }

        @Override
        public void OnWriteConfigCallBack(boolean status) {

        }

        @Override
        public void OnConnected() {

        }

        @Override
        public void OnDisConnected() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(_IsSync) {
                        if (_ProgressDialog != null && _ProgressDialog.isShowing())
                            _ProgressDialog.dismiss();
                        Toast.makeText(GetDataActivity.this, "Disconnected!", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                }
            });
        }

        @Override
        public void OnServicesed(List<BLEGattService> services) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    _ConfigService.CheckToken(Token);
                }
            });
        }

        @Override
        public void OnReadCallBack(UUID uuid, byte[] data) {

        }

        @Override
        public void OnWriteCallBack(UUID uuid,final boolean isSuccess) {
            if (uuid.toString().toLowerCase().equals(new CharacteristicHandle().GetCharacteristicUUID(CharacteristicType.Token).toString().toLowerCase())) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(isSuccess) {
                            if (_SyncMode == 0 && _SyncDateTime == 0)
                                ReadConfig();
                            else {
                                if (_SyncDateTime == 5)
                                    _ConfigService.SetSyncDateTime(_SyncMode, _SyncBeginTime, _SyncEndTime);
                                else
                                    _ConfigService.SetSyncDateTimeMode(_SyncMode, _SyncDateTime);
                            }
                        }else{
                            Toast.makeText(GetDataActivity.this, "password is error", Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    }
                });
            }
            if (uuid.toString().toLowerCase().equals(new CharacteristicHandle().GetCharacteristicUUID(CharacteristicType.SyncMode).toString().toLowerCase())) {
                if (isSuccess) {
                    ReadConfig();
                }
            }
        }

        @Override
        public void OnReceiveCallBack(UUID uuid, byte[] data) {
            DataAnalysis(data);
        }
    };

    public void DataAnalysis(byte[] data) {
        if(_IsSynced)
            return;
        synchronized (this) {
            try {
                //is end packet
                boolean isEnd = false;
                int serial = -1;
                /*******/
                String logMsg = "data:" + StringConvertUtil.bytesToHexString(data) + " => " + StringConvertUtil.hexString2binaryString(StringConvertUtil.bytesToHexString(data));
                /*******/
                if ((HardwareModel.equals("3A01") && Integer.parseInt(Firmware) >= 12)
                        || (HardwareModel.equals("3901") && Integer.parseInt(Firmware) >= 25)
                        || (HardwareModel.equals("3C01") && Integer.parseInt(Firmware) >= 26)
                        || (HardwareModel.equals("3A04") && Integer.parseInt(Firmware) >= 13)) {
                    //begin or end packet
                    if (data.length == 4 && (data[0] == 0x2A || data[0] == 0x24) && data[3] == 0x23) {
                        _SyncCount = Integer.parseInt(StringConvertUtil.bytesToHexString(BinaryUtil.CloneRange(data,1,2)),16);
                        if(_SyncCount == 0)
                            isEnd = true;
                        if(data[0] == 0x24)
                            isEnd = true;
                    }
                    if (_SyncMode == 1) {
                        int dataType = Integer.parseInt(StringUtil.PadLeft(Integer.toBinaryString(data[0]), 8).substring(0, 3),2);
                        serial = Integer.parseInt(StringConvertUtil.hexString2binaryString(StringConvertUtil.bytesToHexString(BinaryUtil.CloneRange(data,0,2))).substring(3),2); //Integer.parseInt(StringConvertUtil.binaryString2hexString("000" + StringUtil.PadLeft(Integer.toBinaryString(data[0]), 8).substring(3) + StringUtil.PadLeft(Integer.toBinaryString(data[1]), 8)),16);
                        //begin packet
                        if (data.length == 4 && dataType == 2) {
                            _SyncCount = Integer.parseInt(StringConvertUtil.bytesToHexString(BinaryUtil.CloneRange(data,2,2)),16);
                            if(_SyncCount == 0)
                                isEnd = true;
                        }
                        //end packet
                        if (data.length == 6 && dataType == 3) {
                            _SyncCount = Integer.parseInt(StringConvertUtil.bytesToHexString(BinaryUtil.CloneRange(data,2,2)),16);
                            isEnd = true;
                        }
                        if (data.length >= 8 && dataType == 1) {
                            _new_sync_datatime = Long.parseLong(StringConvertUtil.bytesToHexString(BinaryUtil.CloneRange(data, 2, 4)), 16);
                            _new_sync_interval = Long.parseLong(StringConvertUtil.bytesToHexString(BinaryUtil.CloneRange(data, 6, 4)), 16);
                        }
                    }else {
                        serial = Integer.parseInt(StringConvertUtil.bytesToHexString(BinaryUtil.CloneRange(data, data.length - 3, 2)),16);
                    }
                }

                List<byte[]> byteslist = _ConfigService.GetDataBytes(data, HardwareModel, Firmware, _SyncMode);
                if (byteslist != null && byteslist.size() > 0){
                    for (byte[] bytes : byteslist) {
                        if (_SyncMode == 1) {
                            Buffer.add(BinaryUtil.MultipleMerge(StringConvertUtil.hexStringToBytes(StringUtil.PadLeft(Long.toHexString(_new_sync_datatime), 8)), bytes));
                            _new_sync_datatime += _new_sync_interval;
                        } else
                            Buffer.add(bytes);
                        _SyncIndex++;
                    }
                }

                final int syncProgress = (int) ((Double.parseDouble(_SyncIndex + "") / _SyncCount) * 100);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(!_IsSynced) {
                            int progress = syncProgress;
                            if ((progress - _SyncProgress) >= 2) {
                                _SyncProgress = progress;
                            }
                        }
                    }
                });

                //end
                if (_SyncIndex >= _SyncCount || isEnd){
                    _SyncProgress = 100;
                    _IsSynced = true;
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ShowData();
                        }
                    });
                }
            } catch (Exception ex) {
                Log.e("SyncAtivity","DataAnalysis => ex:" + ex.toString());
            }
        }
    }

    public String CRC(byte[] data)
    {
        int num = 0;
        for (int i = 0; i < data.length; i++)
            num = (num + data[i]) % 0xffff;
        String hex = Integer.toHexString(num);
        if(hex.length()%2 > 0)
            hex = "0"+hex;
        String crc = hex.substring(hex.length()- 2);
        return  crc;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_get_data, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
